﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Infrastructure
{
    public class StageStatusQueryResult : QueryResult
    {
        public string StageName { get; set; }

        public string Status { get; set; }

        public string DatabaseName { get; set; }

        public int ProcessDetailId { get; set; }

        public int ControlLogId { get; set; }

        public int DetailStatusTypeId { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public int RecordsAffectedCount { get; set; }

        public bool IsRequiredDB { get; set; }

        public string ServerName { get; set; }

        public DateTime CreateDate { get; set; }

    }
}
