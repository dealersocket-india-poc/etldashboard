﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Infrastructure
{
    public class ProcessPerformanceStatisticQueryResult : QueryResult
    {
        public string ProcessName { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public int PerformanceStatisticId { get; set; }

        public int ParentStatisticId { get; set; }

        public int ProcessDetailId { get; set; }

        public int RecordsDeleted { get; set; }

        public int RecordsInserted { get; set; }

        public int RecordsSelected { get; set; }

        public int RecordsUpdated { get; set; }

        public string SuccessfulFlag { get; set; }

        public DateTime CreateDate { get; set; }

        public string CreateUserId { get; set; }

        public DateTime UpdateDate { get; set; }

        public string UpdateUserId { get; set; }
    }
}
