﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Infrastructure
{
    public class StageStatusQuery : Query
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
