﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ETLDashboard.Domain.Models;
using ETLDashboard.Infrastructure;

namespace ETLDashboard.WebApp
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            this.QueryResultToDomainMapper();
        }

        private void QueryResultToDomainMapper()
        {
            CreateMap<StageStatusQueryResult, StageStatus>();

            CreateMap<ProcessPerformanceStatisticQueryResult, PerformanceStatistic>()
                .ForMember(
                    dest => dest.IsSuccessful,
                    opt => opt.MapFrom(src => src.SuccessfulFlag == "Y" ? true : false));
        }
    }
}
