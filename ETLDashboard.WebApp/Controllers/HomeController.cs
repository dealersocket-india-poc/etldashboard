﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ETLDashboard.WebApp.Models;
using ETLDashboard.Services;
using ETLDashboard.Repository;
using ETLDashboard.Domain.Models;
using Newtonsoft.Json;
using AutoMapper;

namespace ETLDashboard.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMapper _mapper;
        private readonly IDWReadRepository _dWReadRepository;
        private readonly IDashboardDataService _dashboardDataService;

        private List<DataPoint> dataPoints1 = new List<DataPoint>();
        private List<DataPoint> dataPoints2 = new List<DataPoint>();
        private List<DataPoint> dataPoints3 = new List<DataPoint>();
        private List<DataPoint> dataPoints4 = new List<DataPoint>();
        private List<DataPoint> dataPoints5 = new List<DataPoint>();
        private List<DataPoint> dataPoints6 = new List<DataPoint>();
        private List<DataPoint> dataPoints7 = new List<DataPoint>();
        private List<DataPoint> dataPoints8 = new List<DataPoint>();
        private List<DataPoint> dataPoints9 = new List<DataPoint>();
        private List<BTreeDataPoint> treeDataPoints = new List<BTreeDataPoint>();
        private List<DataPoint> tDataPoints1 = new List<DataPoint>();
        private List<DataPoint> tDataPoints2 = new List<DataPoint>();
        private List<DataPoint> tDataPoints3 = new List<DataPoint>();
        private List<DataPoint> tDataPoints4 = new List<DataPoint>();
        private List<DataPoint> tDataPoints5 = new List<DataPoint>();
        private List<DataPoint> tDataPoints6 = new List<DataPoint>();
        private List<DataPoint> tDataPoints7 = new List<DataPoint>();
        private List<DataPoint> tDataPoints8 = new List<DataPoint>();
        private List<DataPoint> tDataPoints9 = new List<DataPoint>();
        private List<DetailPerformanceStatisticDataPoint> detailPerformanceStatisticDataPoints = new List<DetailPerformanceStatisticDataPoint>();

        public HomeController(ILogger<HomeController> logger, IMapper mapper, IDWReadRepository dWReadRepository, IDashboardDataService dashboardDataService)
        {
            _logger = logger;
            _mapper = mapper;
            _dWReadRepository = dWReadRepository;
            _dashboardDataService = dashboardDataService;
        }

        public IActionResult Index()
        {
            List<StageStatus> todayData = new List<StageStatus>();
            List<StageStatus> yesterdayData = new List<StageStatus>();
            List<StageStatus> lastWeekData = new List<StageStatus>();

            todayData = this._dashboardDataService.GetStageStatuses().ToList();
            var historicData = this._dashboardDataService.GetStageStatuses(DateTime.Now.AddDays(-7), DateTime.Now);

            foreach (StageStatus element in historicData)
            {
                if (element.StartTime.Date == DateTime.Today.AddDays(-1))
                {
                    yesterdayData.Add(element);
                }
                if (element.StartTime.Date == DateTime.Today.AddDays(-7))
                {
                    lastWeekData.Add(element);
                }
            }

            GenerateChart(todayData, yesterdayData, lastWeekData);
            GenerateTrendChart(historicData);

            return View();
        }

        public IActionResult GetDetails(int processDetailId, int  bucketCount = 5)
        {
            List<PerformanceStatistic> performanceStatistics = this._dashboardDataService.GetPerformanceStatistics(processDetailId).OrderByDescending(x => x.RunTime).Take(bucketCount).ToList(); ;
            this.GenerateBTreeDataPoints(performanceStatistics);
            ViewBag.DetailStatisticsDataPoints = JsonConvert.SerializeObject(detailPerformanceStatisticDataPoints);
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private void GenerateChart(List<StageStatus> todayData, List<StageStatus> yesterdayData, List<StageStatus> lastWeekData)
        {
            var tData = todayData.GroupBy(y => y.ControlLogId).ToList();
            var yData = yesterdayData.GroupBy(y => y.ControlLogId).ToList();
            var lwData = lastWeekData.GroupBy(y => y.ControlLogId).ToList();

            FillDataPoints(tData, "Today");
            FillDataPoints(yData, "Yesterday");
            FillDataPoints(lwData, "Last " + DateTime.Now.AddDays(-7).DayOfWeek.ToString());

            GenerateBTreeDataPoints(todayData);
        }

        private void FillDataPoints(List<IGrouping<int,StageStatus>> stages, string day)
        {
            for (int i = 0; i < stages.Count; i++)
            {
                DateTime sTime = DateTime.Now;
                DateTime eTime = new DateTime();

                foreach (var element in stages[i])
                {
                    if (element.StageName == "STAGING DB CLEANUP")
                        dataPoints1.Add(new DataPoint(day + ", LogId = " + element.ControlLogId, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "DEALER DIRECTOR TO STAGING DB")
                        dataPoints2.Add(new DataPoint(day + ", LogId = " + element.ControlLogId, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "DEALER SITE TO STAGING DB")
                    {
                        sTime = element.StartTime < sTime ? element.StartTime : sTime;
                        eTime = element.EndTime > eTime ? element.EndTime : eTime;
                    }
                    if (element.StageName == "STAGING DB VALIDATION")
                        dataPoints4.Add(new DataPoint(day + ", LogId = " + element.ControlLogId, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "ADDING NEW KPIs-DATA MART LOAD")
                        dataPoints5.Add(new DataPoint(day + ", LogId = " + element.ControlLogId, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "STAGING HISTORY TABLE LOAD")
                        dataPoints6.Add(new DataPoint(day + ", LogId = " + element.ControlLogId, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "DATA MART LOAD")
                        dataPoints7.Add(new DataPoint(day + ", LogId = " + element.ControlLogId, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "FACT TABLE LOAD")
                        dataPoints8.Add(new DataPoint(day + ", LogId = " + element.ControlLogId, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "PROCESS CUBE KPI")
                        dataPoints9.Add(new DataPoint(day + ", LogId = " + element.ControlLogId, element.RunTime.TotalMinutes, element.StageName));
                }
                if (stages[i].Count() > 0)
                    dataPoints3.Add(new DataPoint(day + ", LogId = " + stages[i].Key, (eTime - sTime).TotalMinutes, "DEALER SITE TO STAGING DB"));
            }

            ViewBag.DataPoints1 = JsonConvert.SerializeObject(dataPoints1);
            ViewBag.DataPoints2 = JsonConvert.SerializeObject(dataPoints2);
            ViewBag.DataPoints3 = JsonConvert.SerializeObject(dataPoints3);
            ViewBag.DataPoints4 = JsonConvert.SerializeObject(dataPoints4);
            ViewBag.DataPoints5 = JsonConvert.SerializeObject(dataPoints5);
            ViewBag.DataPoints6 = JsonConvert.SerializeObject(dataPoints6);
            ViewBag.DataPoints7 = JsonConvert.SerializeObject(dataPoints7);
            ViewBag.DataPoints8 = JsonConvert.SerializeObject(dataPoints8);
            ViewBag.DataPoints9 = JsonConvert.SerializeObject(dataPoints9);
        }

        private void GenerateBTreeDataPoints(IEnumerable<StageStatus> stages)
        {
            DateTime sTime = DateTime.Now;
            DateTime eTime = new DateTime();
            string status = string.Empty;
            int processDetailId = 0;

            treeDataPoints.Add(new BTreeDataPoint("PROCESS CUBE KPI", new double[] { 0, 0 }, 0, status));
            treeDataPoints.Add(new BTreeDataPoint("FACT TABLE LOAD", new double[] { 0, 0 }, 0, status));
            treeDataPoints.Add(new BTreeDataPoint("DATA MART LOAD", new double[] { 0, 0 }, 0, status));
            treeDataPoints.Add(new BTreeDataPoint("STAGING HISTORY TABLE LOAD", new double[] { 0, 0 }, 0, status));
            treeDataPoints.Add(new BTreeDataPoint("ADDING NEW KPIs-DATA MART LOAD", new double[] { 0, 0 }, 0, status));
            treeDataPoints.Add(new BTreeDataPoint("STAGING DB VALIDATION", new double[] { 0, 0 }, 0, status));
            treeDataPoints.Add(new BTreeDataPoint("DEALER SITE TO STAGING DB", new double[] { 0, 0 }, 0, status));
            treeDataPoints.Add(new BTreeDataPoint("DEALER DIRECTOR TO STAGING DB", new double[] { 0, 0 }, 0, status));
            treeDataPoints.Add(new BTreeDataPoint("STAGING DB CLEANUP", new double[] { 0, 0 }, 0, status));

            foreach (var element in stages)
            {
                if (element.StageName == "DEALER SITE TO STAGING DB")
                {
                    sTime = element.StartTime < sTime ? element.StartTime : sTime;
                    eTime = element.EndTime > eTime ? element.EndTime : eTime;
                    status = element.Status;
                    processDetailId = element.ProcessDetailId;
                }
                else
                {
                    int index = treeDataPoints.FindIndex(i => i.Label == element.StageName);
                    treeDataPoints[index] = new BTreeDataPoint(element.StageName, new double[] { Convert.ToDouble(element.StartTime.TimeOfDay.ToString().Substring(0, 5).Remove(2, 1)), Convert.ToDouble(element.EndTime.TimeOfDay.ToString().Substring(0, 5).Remove(2, 1)) }, Math.Round(element.RunTime.TotalMinutes, 2), element.Status, element.ProcessDetailId);
                }
            }

            if(stages.Count() > 0)
            {
                treeDataPoints[6] = new BTreeDataPoint("DEALER SITE TO STAGING DB", new double[] { Convert.ToDouble(sTime.TimeOfDay.ToString().Substring(0, 5).Remove(2, 1)), Convert.ToDouble(eTime.TimeOfDay.ToString().Substring(0, 5).Remove(2, 1)) }, Math.Round((eTime - sTime).TotalMinutes, 2), status, processDetailId);
            }

            ViewBag.TreeDataPoints = JsonConvert.SerializeObject(treeDataPoints);

        }

        private void GenerateBTreeDataPoints(IEnumerable<PerformanceStatistic> performanceStatistics)
        {
            DateTime sTime = DateTime.Now;
            DateTime eTime = new DateTime();
            string status = string.Empty;

            foreach (var element in performanceStatistics)
            {
                status = element.EndTime < element.StartTime ? "In Progress" : "Completed";

                detailPerformanceStatisticDataPoints.Add(new DetailPerformanceStatisticDataPoint(element.ProcessName, new double[] { Convert.ToDouble(element.StartTime.TimeOfDay.ToString().Substring(0, 5).Remove(2, 1)), Convert.ToDouble(element.EndTime.TimeOfDay.ToString().Substring(0, 5).Remove(2, 1)) }, Math.Round(element.RunTime.TotalMinutes, 2), status, element.ProcessDetailId));
            }
        }

        private void GenerateTrendChart(IEnumerable<StageStatus> historicData)
        {
            for (int i = 1; i <= 7; i++)
            {
                List<StageStatus> tempData = new List<StageStatus>();
                foreach (StageStatus element in historicData)
                {
                    if (element.StartTime.Date == DateTime.Today.AddDays(-i))
                        tempData.Add(element);
                }
                var tdata = tempData.GroupBy(y => y.ControlLogId).ToList();
                var day = "Last " + DateTime.Now.AddDays(-i).DayOfWeek.ToString();
                FillTrendDataPoints(tdata, day);
            }

            ViewBag.TDataPoints1 = JsonConvert.SerializeObject(tDataPoints1);
            ViewBag.TDataPoints2 = JsonConvert.SerializeObject(tDataPoints2);
            ViewBag.TDataPoints3 = JsonConvert.SerializeObject(tDataPoints3);
            ViewBag.TDataPoints4 = JsonConvert.SerializeObject(tDataPoints4);
            ViewBag.TDataPoints5 = JsonConvert.SerializeObject(tDataPoints5);
            ViewBag.TDataPoints6 = JsonConvert.SerializeObject(tDataPoints6);
            ViewBag.TDataPoints7 = JsonConvert.SerializeObject(tDataPoints7);
            ViewBag.TDataPoints8 = JsonConvert.SerializeObject(tDataPoints8);
            ViewBag.TDataPoints9 = JsonConvert.SerializeObject(tDataPoints9);
        }

        private void FillTrendDataPoints(List<IGrouping<int, StageStatus>> stages, string day)
        {
            for (int i = 0; i < stages.Count; i++)
            {
                DateTime sTime = DateTime.Now;
                DateTime eTime = new DateTime();

                foreach (var element in stages[i])
                {
                    if (element.StageName == "STAGING DB CLEANUP")
                        tDataPoints1.Add(new DataPoint(day, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "DEALER DIRECTOR TO STAGING DB")
                        tDataPoints2.Add(new DataPoint(day, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "DEALER SITE TO STAGING DB")
                    {
                        sTime = element.StartTime < sTime ? element.StartTime : sTime;
                        eTime = element.EndTime > eTime ? element.EndTime : eTime;
                    }
                    if (element.StageName == "STAGING DB VALIDATION")
                        tDataPoints4.Add(new DataPoint(day, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "ADDING NEW KPIs-DATA MART LOAD")
                        tDataPoints5.Add(new DataPoint(day, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "STAGING HISTORY TABLE LOAD")
                        tDataPoints6.Add(new DataPoint(day, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "DATA MART LOAD")
                        tDataPoints7.Add(new DataPoint(day, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "FACT TABLE LOAD")
                        tDataPoints8.Add(new DataPoint(day, element.RunTime.TotalMinutes, element.StageName));
                    if (element.StageName == "PROCESS CUBE KPI")
                        tDataPoints9.Add(new DataPoint(day, element.RunTime.TotalMinutes, element.StageName));
                }
                if (stages[i].Count() > 0)
                    tDataPoints3.Add(new DataPoint(day, (eTime - sTime).TotalMinutes, "DEALER SITE TO STAGING DB"));
            }
        }
    }

}
