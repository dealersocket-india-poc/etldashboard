﻿using ETLDashboard.Repository;
using ETLDashboard.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ETLDashboard.WebApp.Plumbing
{
    public static class DependencyInstaller
    {
        public static void Install(IServiceCollection services)
        {
            // add the injections here
            services.AddTransient<IDashboardDataService, DashboardDataService>();
            services.AddTransient<IDWReadRepository, DWReadRepository>();
        }
    }
}
