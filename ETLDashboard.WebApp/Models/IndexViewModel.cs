﻿using System;
using System.Runtime.Serialization;

namespace ETLDashboard.WebApp.Models
{
	[DataContract]
	public class DataPoint
	{
		public DataPoint(string label, double y, string stage)
		{
			this.Label = label;
			this.Y = y;
			this.Stage = stage;
		}

		[DataMember(Name = "label")]
		public string Label = "";

		[DataMember(Name = "y")]
		public Nullable<double> Y = null;

		[DataMember(Name = "stage")]
		public string Stage = null;
	}

	[DataContract]
	public class BTreeDataPoint
	{
		public BTreeDataPoint(string label, double[] y, double runtime, string status, int processDetailId = 0)
		{
			this.Label = label;
			this.Y = y;
			this.RunTime = runtime;
			this.Status = status;
			this.ProcessDetailId = processDetailId;
		}

		[DataMember(Name = "label")]
		public string Label = "";

		[DataMember(Name = "y")]
		public double[] Y = null;

		[DataMember(Name = "runtime")]
		public Nullable<double> RunTime = null;

		[DataMember(Name = "status")]
		public string Status = null;

		[DataMember(Name = "processDetailId")]
		public int ProcessDetailId;
	}
}
