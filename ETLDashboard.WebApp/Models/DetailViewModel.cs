﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace ETLDashboard.WebApp.Models
{
	[DataContract]
	public class DetailPerformanceStatisticDataPoint
	{
		public DetailPerformanceStatisticDataPoint(string label, double[] y, double runtime, string status, int processDetailId = 0)
		{
			this.Label = label;
			this.Y = y;
			this.RunTime = runtime;
			this.Status = status;
			this.ProcessDetailId = processDetailId;
		}

		[DataMember(Name = "label")]
		public string Label = "";

		[DataMember(Name = "y")]
		public double[] Y = null;

		[DataMember(Name = "runtime")]
		public Nullable<double> RunTime = null;

		[DataMember(Name = "status")]
		public string Status = null;

		[DataMember(Name = "processDetailId")]
		public int ProcessDetailId;
	}
}
