﻿using ETLDashboard.Domain.Models;
using ETLDashboard.Infrastructure;
using ETLDashboard.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;

namespace ETLDashboard.Services
{
    public class DashboardDataService : IDashboardDataService
    {
        private readonly IDWReadRepository _dwReadRepository;
        private readonly IMapper _mapper;

        public DashboardDataService(IDWReadRepository iDWReadRepository, IMapper mapper)
        {
            this._dwReadRepository = iDWReadRepository;
            this._mapper = mapper;
        }

        public IEnumerable<PerformanceStatistic> GetPerformanceStatistics(int processDetailId)
        {
            ProcessPerformanceStatisticQuery processPerformanceStatisticQuery = new ProcessPerformanceStatisticQuery()
            {
                ProcessDetailId = processDetailId
            };

            var queryResults = this._dwReadRepository.GetProcessPerformanceStatistic(processPerformanceStatisticQuery);

            List<PerformanceStatistic> performanceStatistic = this._mapper.Map<List<PerformanceStatistic>>(queryResults);

            return performanceStatistic;
        }

        public IEnumerable<StageStatus> GetStageStatuses(DateTime startDate, DateTime endDate)
        {
            StageStatusQuery stageStatusQuery = new StageStatusQuery()
            {
                StartDate = startDate,
                EndDate = endDate
            };

            var queryResults = this._dwReadRepository.GetHistoricStageSatus(stageStatusQuery);

            List<StageStatus> stageStatuses = this._mapper.Map<List<StageStatus>>(queryResults);

            return stageStatuses;
        }

        public IEnumerable<StageStatus> GetStageStatuses()
        {
            StageStatusQuery stageStatusQuery = new StageStatusQuery()
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now
            };

            var queryResults = this._dwReadRepository.GetCurrentStageStatus(stageStatusQuery);

            List<StageStatus> stageStatuses = this._mapper.Map<List<StageStatus>>(queryResults);

            return stageStatuses;
        }
    }
}
