﻿using ETLDashboard.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Services
{
    public interface IDashboardDataService : IDataService
    {
        IEnumerable<StageStatus> GetStageStatuses(DateTime startDate, DateTime endDate);

        IEnumerable<StageStatus> GetStageStatuses();

        IEnumerable<PerformanceStatistic> GetPerformanceStatistics(int processDetailId);
    }
}
