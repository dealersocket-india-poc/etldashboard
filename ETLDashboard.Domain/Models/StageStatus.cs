﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Domain.Models
{
    public class StageStatus
    {
        public string StageName { get; set; }

        public string Status { get; set; }

        public string DatabaseName { get; set; }

        public int ProcessDetailId { get; set; }

        public int ControlLogId { get; set; }

        public int DetailStatusTypeId { get; set; }

        private DateTime _startTime;
        public DateTime StartTime
        {
            get
            {
                return _startTime;
            }
            set
            {
                _startTime = value == new DateTime() ? TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "Mountain Standard Time") : value;
            }
        }

        private DateTime _endTime;
        public DateTime EndTime
        {
            get
            {
                return _endTime;
            }
            set
            {
                _endTime = value == new DateTime() ? TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "Mountain Standard Time") : value;
            }
        }

        public int RecordsAffectedCount { get; set; }

        public bool IsRequiredDB { get; set; }

        public string ServerName { get; set; }

        public DateTime CreateDate { get; set; }

        public TimeSpan RunTime
        {
            get
            {
                return EndTime - StartTime;
            }
        }
    }
}
