﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Domain.Models
{
    public class PerformanceStatistic
    {
        private DateTime _startTime;

        private DateTime _endTime;

        public string ProcessName { get; set; } // this will go away

        public int PerformanceStatisticId { get; set; }

        public int ParentStatisticId { get; set; }

        public int ProcessDetailId { get; set; }

        public int RecordsDeleted { get; set; }

        public int RecordsInserted { get; set; }

        public int RecordsSelected { get; set; }

        public int RecordsUpdated { get; set; }

        public bool IsSuccessful { get; set; }
         
        public DateTime CreateDate { get; set; }

        public string CreateUserId { get; set; }

        public DateTime UpdateDate { get; set; }

        public string UpdateUserId { get; set; }

        public DateTime StartTime
        {
            get
            {
                return _startTime;
            }
            set
            {
                _startTime = value == new DateTime() ? TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "Mountain Standard Time") : value;
            }
        }

        public DateTime EndTime
        {
            get
            {
                return _endTime;
            }
            set
            {
                _endTime = value == new DateTime() ? TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "Mountain Standard Time") : value;
            }
        }

        public TimeSpan RunTime
        {
            get
            {
                return EndTime - StartTime;
            }
        }
    }
}
