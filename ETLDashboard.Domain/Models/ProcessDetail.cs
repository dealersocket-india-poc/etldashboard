﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Domain.Models
{
    public class ProcessDetail
    {
        public int ProcessId { get; set; }

        public string ProcessName { get; set; }

        public string ProcessType { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public TimeSpan RunTime
        {
            get
            {
                return EndTime - StartTime;
            }
        }

        public PerformanceStatistic PerformanceStatistic { get; set; }
    }
}
