﻿using ETLDashboard.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Repository
{
    public interface IDWReadRepository : IReadRepository
    {
        List<StageStatusQueryResult> GetHistoricStageSatus(StageStatusQuery query);

        List<StageStatusQueryResult> GetCurrentStageStatus(StageStatusQuery query);

        List<ProcessPerformanceStatisticQueryResult> GetProcessPerformanceStatistic(ProcessPerformanceStatisticQuery query);
    }
}
