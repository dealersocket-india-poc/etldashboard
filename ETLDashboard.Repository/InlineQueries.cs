﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Repository
{
    public static class InlineQueries
    {
        private const string _historicDataCmd = @"SELECT 
        pt.LoadProcessTypeDescription AS 'StageName'
       , t.DetailStatusTypeDescription AS 'Status'
	   , t.DetailStatusTypeId
       ,sd.DatabaseName
       ,d.ProcessDetailId
       ,d.ControlLogId
       ,d.DetailStatusTypeId
       ,d.EndTime
       ,d.RecordsAffectedCount
       ,d.StartTime
       ,d.CreateDate
       ,sInfo.ServerName
        ,sd.IsRequiredDB
       FROM dbo.LoadProcessDetail AS d with(nolock)
       INNER JOIN LoadProcessHeader AS h with(nolock)  ON d.ProcessHeaderId = h.ProcessHeaderId
       INNER JOIN DetailStatusType  AS t with (nolock) ON d.DetailStatusTypeId = t.DetailStatusTypeId

       INNER JOIN ServerDatabaseAssociation AS src with (nolock) ON h.SourceServerDBAssociationId = src.ServerDBAssociationId

       INNER JOIN ServerDatabaseAssociation AS dest with (nolock) ON h.DestinationServerDBAssociationId = dest.ServerDBAssociationId

       INNER JOIN DatabaseInformation AS sd with (nolock) ON src.DatabaseId = sd.DatabaseId

       INNER JOIN DatabaseInformation dd with (nolock) ON dest.DatabaseId = dd.DatabaseId

       INNER JOIN LoadProcessType AS pt with (nolock) ON h.LoadProcessTypeId = pt.LoadProcessTypeId

       INNER JOIN ServerInformation AS sInfo with (nolock) ON sInfo.serverId = src.serverId

       WHERE
       
        d.CreateDate between 'paramBeginDate' and 'paramEndDate'";

        private const string _currentDataCmd = @"DECLARE @ControlLogId int   

        SELECT TOP 1
        @ControlLogId = c.ControlLogId
        FROM    ControlLog AS c with(nolock)
        INNER JOIN ControlLogStatusType AS t with(nolock) ON c.ControlLogStatusTypeId = t.ControlLogStatusTypeId
        ORDER BY ControlLogId DESC

        SELECT 
	    pt.LoadProcessTypeDescription AS 'StageName'
       ,t.DetailStatusTypeDescription AS 'Status'
	   ,t.DetailStatusTypeId
       ,sd.DatabaseName
       ,d.ProcessDetailId
       ,d.ControlLogId
       ,d.DetailStatusTypeId
       ,d.EndTime
       ,d.RecordsAffectedCount
       ,d.StartTime
       ,d.CreateDate
       ,sInfo.ServerName
       ,sd.IsRequiredDB
        FROM    dbo.LoadProcessDetail  AS d with (nolock) 
        INNER JOIN LoadProcessHeader AS h with (nolock)  ON d.ProcessHeaderId = h.ProcessHeaderId
        INNER JOIN DetailStatusType  AS t with (nolock)  ON d.DetailStatusTypeId = t.DetailStatusTypeId
        INNER JOIN ServerDatabaseAssociation AS src with (nolock)  ON h.SourceServerDBAssociationId = src.ServerDBAssociationId
        INNER JOIN ServerDatabaseAssociation AS dest with (nolock)  ON h.DestinationServerDBAssociationId = dest.ServerDBAssociationId
        INNER JOIN DatabaseInformation AS sd with (nolock)  ON src.DatabaseId = sd.DatabaseId
        INNER JOIN DatabaseInformation dd with (nolock)  ON dest.DatabaseId = dd.DatabaseId
        INNER JOIN LoadProcessType AS pt with (nolock)  ON h.LoadProcessTypeId = pt.LoadProcessTypeId
        INNER JOIN ServerInformation AS sInfo with (nolock)  ON sInfo.serverId = src.serverId
        WHERE   d.ControlLogId = @ControlLogId";

        private const string _processPerformanceStatisticCmd = @"
        select 
        PerformanceStatisticId,
        ParentStatisticId,
        ProcessDetailId,
        ProcessName,
        StartTime,
        EndTime,
        RecordsDeleted,
        RecordsInserted,
        RecordsSelected,
        RecordsUpdated,
        SuccessfulFlag,
        Parameters,
        CreateDate,
        CreateUserId,
        UpdateDate,
        UpdateUserId
        from EDRPerformanceStatistic with(nolock)  where  ProcessDetailId = paramProcessDetailId";

        public static string HistoricDataCmd(DateTime beginDate, DateTime endDate)
        {
            var cmd = _historicDataCmd;

            cmd = cmd.Replace("paramBeginDate", beginDate.ToString("yyyy/MM/dd"));
            cmd = cmd.Replace("paramEndDate", endDate.ToString("yyyy/MM/dd"));

            return cmd;
        }

        public static string CurrentDataCmd()
        {
            return _currentDataCmd;
        }

        public static string ProcessPerformanceStatisticCmd(int processDetailId)
        {
            var cmd = _processPerformanceStatisticCmd;

            cmd = cmd.Replace("paramProcessDetailId", processDetailId.ToString());

            return cmd;
        }
    }
}