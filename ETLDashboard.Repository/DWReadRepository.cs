﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using Dapper;
using ETLDashboard.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace ETLDashboard.Repository
{
    public class DWReadRepository : IDWReadRepository
    {
        private readonly Cache _cache;
        private readonly string _conStr;

        public DWReadRepository(IConfiguration configuration)
        {
            this._conStr = configuration["ConStr"];
            this._cache = Cache.GetInstance();
        }
   
        public List<StageStatusQueryResult> GetCurrentStageStatus(StageStatusQuery query)
        {
            List<StageStatusQueryResult> queryResults;

            using (var connection = new SqlConnection(this._conStr))
            {
                queryResults = connection.Query<StageStatusQueryResult>(InlineQueries.CurrentDataCmd()).ToList();
            }

            return queryResults;
        }

        public List<StageStatusQueryResult> GetHistoricStageSatus(StageStatusQuery query)
        {
            List<StageStatusQueryResult> queryResults;

            if (_cache.TryGetHistoricData(out queryResults))
            {
                return queryResults;
            }

            using (var connection = new SqlConnection(this._conStr))
            {
                queryResults = connection.Query<StageStatusQueryResult>(InlineQueries.HistoricDataCmd(query.StartDate, query.EndDate)).ToList();
            }

            _cache.UpdateHistoricData(queryResults);
            return queryResults;
        }

        public List<ProcessPerformanceStatisticQueryResult> GetProcessPerformanceStatistic(ProcessPerformanceStatisticQuery query)
        {
            List<ProcessPerformanceStatisticQueryResult> queryResults;

            using (var connection = new SqlConnection(this._conStr))
            {
                queryResults = connection.Query<ProcessPerformanceStatisticQueryResult>(InlineQueries.ProcessPerformanceStatisticCmd(query.ProcessDetailId)).ToList();
            }

            return queryResults;
        }
    }
}