﻿using ETLDashboard.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Repository
{
    public class Cache
    {
        private static Cache _cacheInstance;
        private static List<StageStatusQueryResult> _historicStageStatusQueryResults = new List<StageStatusQueryResult>();
        private static DateTime _lastUpdated;

        private Cache() { }

        public static Cache GetInstance()
        {
            if (_cacheInstance == null)
            {
                return new Cache();
            }

            return _cacheInstance;
        }

        public bool TryGetHistoricData(out List<StageStatusQueryResult> stageStatuses)
        {
            stageStatuses = _historicStageStatusQueryResults;

            return _historicStageStatusQueryResults.Count > 0 && _lastUpdated == DateTime.Today ? true : false; 
        }

        public void UpdateHistoricData(List<StageStatusQueryResult> stageStatuses)
        {
            _historicStageStatusQueryResults = stageStatuses;
            _lastUpdated = DateTime.Today;
        }
    }
}
