﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Common.Enum
{
    public enum ControlLogStatuscs
    {
        NTSTD = 1,
        STGST,
        STGFD,
        CLNST,
        CLNFD,
        DMTST,
        DMTFD,
        COMPL,
        CMPER,
        FCTST,
        FCTFD,
        PCKST,
        PCKFD,
        RSANK,
        RSSHL,
        RSDML,
        RSFTL,
        RSPCK,
    }
}
