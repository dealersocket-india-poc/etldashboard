﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETLDashboard.Common.Enum
{
    public enum DetailStatus
    {
        NOTST = 1,
        INPRG,
        CTSUC,
        CTERR
    }
}
